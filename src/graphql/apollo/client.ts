import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
} from '@apollo/client/core';

import { typePolicies } from './store';

// HTTP connection to the API
const httpLink = createHttpLink({
  uri: `${import.meta.env.VITE_GRAPHQL_API}`,
});

// Cache implementation
const cache = new InMemoryCache({ typePolicies });

// Create the apollo client
const apolloClient = new ApolloClient({
  link: httpLink,
  cache,
  connectToDevTools: true,
});

export default apolloClient;
