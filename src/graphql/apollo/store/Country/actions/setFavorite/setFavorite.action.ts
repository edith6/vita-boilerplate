import { inject } from 'vue';

import { ApolloClient } from '@apollo/client/core';

import { DefaultApolloClient } from '@vue/apollo-composable';

import {
  Country,
  CountryFieldsFragmentDoc,
  FavoriteCountriesDocument,
  FavoriteCountriesQuery,
} from '../../../../../generated/graphql';

type SetFavoriteCountryAction = (
  countryId: Country['id'],
  favorite: boolean,
) => void;

export const useSetFavoriteCountryAction = (): SetFavoriteCountryAction => {
  const client = inject<ApolloClient<unknown>>(DefaultApolloClient);

  if (client instanceof ApolloClient) {
    return (id, favorite) => {
      const country = client.readFragment<Country>({
        id: client.cache.identify({
          __typename: 'Country',
          id,
        }),
        fragment: CountryFieldsFragmentDoc,
      });

      if (country) {
        const favoriteCountriesQuery = client.readQuery<FavoriteCountriesQuery>(
          {
            query: FavoriteCountriesDocument,
          },
        );

        const existingFavoriteCountries =
          favoriteCountriesQuery?.favoriteCountries ?? [];

        const favoriteCountries = favorite
          ? [...existingFavoriteCountries, country]
          : existingFavoriteCountries.filter((country) => country.id !== id);

        client.writeQuery<FavoriteCountriesQuery>({
          query: FavoriteCountriesDocument,
          data: {
            favoriteCountries,
          },
        });
      }
    };
  }

  throw new Error('ApolloClient was not privided!');
};
