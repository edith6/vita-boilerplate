import { offsetLimitPagination } from '@apollo/client/utilities';

import { QueryFieldPolicy } from '../../../../generated/apollo-helpers';

const CountryQueries: QueryFieldPolicy = {
  favoriteCountries: {
    merge: false,
  },
  countries: offsetLimitPagination(['where']),
};

export default CountryQueries;
