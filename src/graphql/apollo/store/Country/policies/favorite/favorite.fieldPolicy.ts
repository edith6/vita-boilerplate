import type { CountryFieldPolicy } from '../../../../../generated/apollo-helpers';

import {
  Country,
  FavoriteCountriesIdsDocument,
  FavoriteCountriesIdsQuery,
} from '../../../../../generated/graphql';

export const favorite: CountryFieldPolicy['favorite'] = {
  read: (_, { cache, readField }) => {
    const id = readField<Country['id']>('id');

    const favoriteCountriesQuery = cache.readQuery<FavoriteCountriesIdsQuery>({
      query: FavoriteCountriesIdsDocument,
    });

    const favorite = favoriteCountriesQuery?.favoriteCountries.some(
      (country) => country.id === id,
    );

    return favorite ?? false;
  },
};
