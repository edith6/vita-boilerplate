import { TypedTypePolicies } from '../../../../generated/apollo-helpers';

import { favorite } from './favorite/favorite.fieldPolicy';

const Country: NonNullable<TypedTypePolicies['Country']> = {
  fields: {
    favorite,
  },
};

export default Country;
