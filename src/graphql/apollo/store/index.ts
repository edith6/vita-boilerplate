import type { TypedTypePolicies } from '../../generated/apollo-helpers';

import Country from './Country/policies';

import Query from './Query/policies';

export const typePolicies: NonNullable<TypedTypePolicies> = {
  Country,
  Query,
};
