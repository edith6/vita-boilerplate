import { TypedTypePolicies } from '../../../../generated/apollo-helpers';

import CountryQueries from '../../Country/queries';

const Query: NonNullable<TypedTypePolicies['Query']> = {
  fields: {
    ...CountryQueries,
  },
};

export default Query;
