import { createApp } from 'vue';

import App from './components/views/App/App.vue';

import './main.scss';

createApp(App).mount('#app');
